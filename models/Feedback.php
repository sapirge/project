<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property string $body
 * @property int $rating
 * @property string $auth_key
 * @property string $responder_name
 * @property string $created_at
 * @property int $recipe_id
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rating', 'recipe_id'], 'integer'],
            [['created_at'], 'safe'],
            [['body', 'auth_key', 'responder_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'body' => 'Body',
            'rating' => 'Rating',
            'auth_key' => 'Auth Key',
            'responder_name' => 'Responder Name',
            'created_at' => 'Created At',
            'recipe_id' => 'Recipe ID',
        ];
    }
}
