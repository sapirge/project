<?php

use yii\db\Migration;

/**
 * Handles the creation of table `feedback`.
 */
class m180726_152552_create_feedback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('feedback', [
            'id' => $this->primaryKey(),
            'body'=> $this->string(),
            'rating'=> $this->integer(),
            'auth_key' => $this->string(),
            'responder_name'=> $this->string(),
            'created_at' => $this->timestamp(),
             'recipe_id'=> $this->integer(),
            
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('feedback');
    }
}
