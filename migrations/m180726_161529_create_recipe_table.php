<?php

use yii\db\Migration;

/**
 * Handles the creation of table `recipe`.
 */
class m180726_161529_create_recipe_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('recipe', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
            'body'=> $this->text(),
            'rating'=> $this->integer(),
            'category'=> $this->string(),
            'auth_key' => $this->string(),
            'created_at' => $this->timestamp(),
           'updated_at' => $this->timestamp(),
           'created_by' => $this->integer(),
           'updated_by' => $this->integer(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('recipe');
    }
}
